library IEEE;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity ledc8x8 is
	port(
		SMCLK: in std_logic;
		RESET: in std_logic;
		ROW: out std_logic_vector (0 to 7);
		LED: out std_logic_vector (0 to 7)	
	);
end entity ledc8x8;

architecture behaviour of ledc8x8 is
	signal ctrl_cnt: STD_LOGIC_VECTOR (21 downto 0);
	signal row_cnt: STD_LOGIC_VECTOR (0 to 7) := "10000000";
	signal wut: STD_LOGIC_VECTOR (0 to 7);	--otázniky
	signal switch: STD_LOGIC;
	signal ce: STD_LOGIC;					--clock enable
begin

sig_div: process (RESET, SMCLK)				--delime signál
begin
	if RESET = '1' then
		ctrl_cnt <= "0000000000000000000000";
	elsif SMCLK'event and SMCLK = '1' then
		ctrl_cnt <= ctrl_cnt + 1;
		if ctrl_cnt(7 downto 0) = "11111111" then 
			ce <= '1';
		else ce <= '0';
		end if;
	end if;
	
	switch <= ctrl_cnt(21);
end process;

lineAct_rotate: process (RESET, SMCLK)				--aktivácia riadku
begin
	if RESET = '1'  then
		row_cnt <= "10000000";
	
	elsif SMCLK'event and SMCLK = '1' then 
		if ce = '1' then
			case row_cnt is         		-- rotujeme nech to nie je otočené naopak
				when "00000001" => row_cnt <= "10000000";
				when "00000010" => row_cnt <= "00000001";
				when "00000100" => row_cnt <= "00000010";
				when "00001000" => row_cnt <= "00000100";
				when "00010000" => row_cnt <= "00001000";
				when "00100000" => row_cnt <= "00010000";
				when "01000000" => row_cnt <= "00100000";
				when "10000000" => row_cnt <= "01000000";	
				when others => null;
			end case;
		end if;
	end if;		
end process;

inic: process (row_cnt)						--iniciálky
begin
	
	case row_cnt is         
		when "10000000" => wut <= "00001111";
		when "01000000" => wut <= "11101111";
		when "00100000" => wut <= "11101111";
		when "00010000" => wut <= "10100001";
		when "00001000" => wut <= "11000110";
		when "00000100" => wut <= "11110001";
		when "00000010" => wut <= "11110110";
		when "00000001" => wut <= "11110001";	
		when others => null;
	end case;
end process;

flicker_show: process(row_cnt)				--(actual)blikanie
begin
	ROW <= row_cnt;
	if switch = '1' then
		LED <= wut;
	else LED <= "00000000";
	end if;
end process;

end architecture behaviour;	




















